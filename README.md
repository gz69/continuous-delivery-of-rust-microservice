
# Guess The Number: Rust Microservice

A simple Rust-based microservice that serves a guessing game through a REST API. Players guess a number between 1 and 10, and the service responds whether the guess is correct. This project includes Docker configuration for easy containerization and deployment, and it's set up for CI/CD with GitLab.

## Demo Video

Check out the demonstration of this project in action [here](#).

## Prerequisites

Before starting, make sure you have Rust and Cargo installed on your system. You'll also need Docker for containerization. If you plan to use the CI/CD pipeline, having GitLab set up is necessary.

## Setup and Running Locally

### Create a New Rust Project

Initialize a new Rust project using Cargo:

```bash
cargo new guess_the_number && cd guess_the_number
```

### Add Dependencies

Update your `Cargo.toml` file with the necessary dependencies:

```toml
[dependencies]
actix-web = "4"
rand = "0.8"
serde = { version = "1.0", features = ["derive"] }
```

### Implement Functionality

In your `main.rs`, implement the core functionality as described. Ensure your application logic to guess a number between 1 and 10 is correctly implemented.

### Run the Application

To run your application locally and test it:

```bash
cargo run
```

Navigate to `http://localhost:8080` to interact with your application.

## Containerization with Docker

Create a `Dockerfile` in your project directory:

```Dockerfile
FROM rust:latest as builder
WORKDIR /usr/src/guess_the_number
COPY . .
RUN cargo install --path .

FROM debian:buster-slim
COPY --from=builder /usr/local/cargo/bin/guess_the_number /usr/local/bin/guess_the_number
EXPOSE 8080
CMD ["guess_the_number"]
```

Build and run your Docker container:

```bash
docker build -t guess_the_number_service .
docker run -p 8080:8080 guess_the_number_service
```

## Continuous Integration and Deployment

### GitLab CI/CD

Add a `.gitlab-ci.yml` file to your project root to define the CI/CD pipeline. Adjust the following template to fit your needs:

```yaml
image: docker:25.0.3

variables:
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  DOCKER_HOST: tcp://docker:2375
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: ""

services:
  - docker:dind

stages:
  - test

test:
  stage: test
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker build -t guess_number_service .
    - docker run -d -p 8080:8080 guess_number_service
    - docker ps -a
```

This configuration will build and run your Docker image on every push to your GitLab repository, allowing for continuous testing.

## Final Notes

Ensure all your steps are correctly implemented and tested. For more complex CI/CD workflows or deployment strategies, consider customizing the `.gitlab-ci.yml` file further.

Good luck with your Rust microservice, and enjoy the guessing game!
